# lifer_GOT

Game Of Three from Takeaway.com built using the [Java WebSocket API (JSR 356)](jcp.org/en/jsr/detail?id=356).

## Build & run

This is a Standard Maven project

- Requirements = JDK 1.8, maven
- To build, execute `mvn clean install`, will produce independent client/server JAR in target dir
- Run it using `java -jar *.jar` (client/server)

## How to run the game
- Build from source or from target/ directory download binaries (game_client.jar & game_server.jar)
- Launch Server
- Launch an instance of client as player 1 (id will be assigned automatically)
- Launch another player, server will start the game between 2 players.
- Server will moderate and announce final results.

## Samples
- Server

```Jan 10, 2021 9:07:45 PM org.glassfish.grizzly.http.server.NetworkListener start
   INFO: Started listener bound to [0.0.0.0:8088]
   Jan 10, 2021 9:07:45 PM org.glassfish.grizzly.http.server.HttpServer start
   INFO: [HttpServer] Started.
   Jan 10, 2021 9:07:45 PM org.glassfish.tyrus.server.Server start
   INFO: WebSocket Registered apps: URLs all start with ws://localhost:8088
   Jan 10, 2021 9:07:45 PM org.glassfish.tyrus.server.Server start
   INFO: WebSocket server started.
   Game Server started.!
   Waiting for players to join.
   New player registered : p57
   New player registered : p107
   Game Started, 663 sent to player p107
   p107 played : 221
   p57 played : 74
   p107 played : 25
   p57 played : 8
   p107 played : 3
   p57 played : 1
   p57 WON. GAME OVER..!
   End the game.!
   Close all player sessions.
   Game server Stopped.
```

- Player 1
```Connecting to Game with ws://localhost:8088/game/p57/
   Player p57 connected to Game
   Received : Welcome p57
   Received : 221
   (221 +1) /3 = 74
   Sending : 74
   Received : 25
   (25 -1) /3 = 8
   Sending : 8
   Received : 3
   (3 ) /3 = 1
   p57 Won..!
   Received : Game Over..!
   Received : Player p107 has quit the game
```
- Player 2
```Connecting to Game with ws://localhost:8088/game/p107/
   Player p107 connected to Game
   Received : Welcome p107
   Received : 663
   (663 ) /3 = 221
   Sending : 221
   Received : 74
   (74 +1) /3 = 25
   Sending : 25
   Received : 8
   (8 +1) /3 = 3
   Sending : 3
   Received : Game Over..!
   Received : You lose, thanks for playing.!
```
- Build Report
```-------------------------------------------------------------------------------
   Test set: com.taway.got.playerClient.GameServiceTest
   -------------------------------------------------------------------------------
   Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.09 sec
```

- Contact for support
```
venkat.pandey@gmx.com
```
