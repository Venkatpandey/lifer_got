package com.taway.got.playerClient;

import com.taway.got.exceptions.InvalidMoveException;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class GameServiceTest {

	@Test public void calculateNextMove() throws InvalidMoveException {
		GameService gameService =  new GameService(3);

		int current = 56;
		final Map<String, String> result = gameService.calculateNextMove(current);
		assertEquals("is not divisible by 3", "+1", result.get("finalMove"));
		assertEquals("is not divisible by 3", 19, Integer.parseInt(result.get("nextMove")));

		current = Integer.parseInt(result.get("nextMove"));
		final Map<String, String> result2 = gameService.calculateNextMove(current);
		assertEquals("is not divisible by 3", "-1", result2.get("finalMove"));
		assertEquals("is not divisible by 3", 6, Integer.parseInt(result2.get("nextMove")));

		current = Integer.parseInt(result2.get("nextMove"));
		final Map<String, String> result3 = gameService.calculateNextMove(current);
		assertEquals("is not divisible by 3", "", result3.get("finalMove"));
		assertEquals("is not divisible by 3", 2, Integer.parseInt(result3.get("nextMove")));

		current = Integer.parseInt(result3.get("nextMove"));
		final Map<String, String> result4 = gameService.calculateNextMove(current);
		assertEquals("is not divisible by 3", "+1", result4.get("finalMove"));
		assertEquals("is not divisible by 3", 1, Integer.parseInt(result4.get("nextMove")));
	}

	@Test(expected = InvalidMoveException.class)
	public void calculateNextMoveInvalidException() throws InvalidMoveException {
		GameService gameService =  new GameService(3);

		int current = -56;
		final Map<String, String> result = gameService.calculateNextMove(current);
	}
}
