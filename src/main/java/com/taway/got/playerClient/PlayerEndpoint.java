package com.taway.got.playerClient;

import com.taway.got.exceptions.InvalidMoveException;
import com.taway.got.utils.GameUtility;

import javax.websocket.*;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@ClientEndpoint public class PlayerEndpoint {

	private String player;
	private CountDownLatch controlLatch = new CountDownLatch(2);
	private GameService gameService;

	public PlayerEndpoint(String player, int divisor) {
		this.player = player;
		this.gameService = new GameService(divisor);
	}

	@OnOpen public void connected(Session session, EndpointConfig clientConfig) {
		ClientEndpointConfig clientConfigs = (ClientEndpointConfig) clientConfig;
		System.out.println("Player " + player + " connected to Game");
	}

	@OnMessage public void playMove(String message, Session session) throws InterruptedException, InvalidMoveException {
		System.out.println("Received : " + message);

		if (GameUtility.isNumeric(message)) {
			int current = GameUtility.toInteger(message);
			final Map<String, String> result = gameService.calculateNextMove(current);
			int nextMove = GameUtility.toInteger(result.get("nextMove"));
			if (nextMove != GameUtility.GAME_POINT) {
				showMoves(current, result, nextMove);
				System.out.println("Sending : " + nextMove);
			} else {
				showMoves(current, result, nextMove);
				System.out.println(player + " Won..!");
				//session.close();
			}
			session.getAsyncRemote().sendText(GameUtility.toString(nextMove));
			controlLatch.await(3, TimeUnit.SECONDS);
		}
	}

	private void showMoves(int current, Map<String, String> result, int nextMove) {
		System.out.println("(" + current +" "+ result.get("finalMove") + ") / " +
			gameService.getDivisor() +" = "+ nextMove);
	}

	@OnError public void playError(Throwable t) {
		t.printStackTrace();
	}
}
