package com.taway.got.playerClient;

import com.taway.got.exceptions.InvalidMoveException;
import com.taway.got.utils.GameUtility;

import java.util.HashMap;
import java.util.Map;

public class GameService {

	private static String SIGN_ADD = "+1";
	private static String SIGN_SUBTRACT = "-1";
	private static String SIGN_NULL = "";
	private int divisor;
	private String finalMove;
	private int nextMove;

	public GameService(int divisor) {
		this.divisor = divisor;
	}

	public int getDivisor() {
		return divisor;
	}

	public Map<String, String> calculateNextMove(int current) throws InvalidMoveException {
		if (current == 1) {
			nextMove = current;
		} else if (current < 1) {
			throw new InvalidMoveException(GameUtility.toString(current));
		}

		if (current % divisor == 0) {
			nextMove = current / divisor;
			finalMove = SIGN_NULL;
		} else if ((current+1) % divisor == 0) {
			nextMove = (current+1) / divisor;
			finalMove = SIGN_ADD;
		} else if ((current-1) % divisor == 0 ) {
			nextMove = (current-1) / divisor;
			finalMove = SIGN_SUBTRACT;
		} else {
			throw new InvalidMoveException(GameUtility.toString(current));
		}

		return calculatedResult();
	}

	private Map<String, String> calculatedResult() {
		Map<String, String> result = new HashMap<>();
		result.put("finalMove", finalMove);
		result.put("nextMove", GameUtility.toString(nextMove));

		return result;
	}

}
