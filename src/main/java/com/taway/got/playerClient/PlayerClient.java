package com.taway.got.playerClient;

import com.taway.got.utils.GameUtility;
import com.taway.got.utils.SystemPropertyLoader;

import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.WebSocketContainer;
import java.io.IOException;
import java.net.URI;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.CountDownLatch;

public class PlayerClient {

	final static CountDownLatch messageLatch = new CountDownLatch(1);

	public static void main(String[] args) {
		int result = getPlayerId()+getPlayerId();
		try {
			WebSocketContainer container = ContainerProvider.getWebSocketContainer();
			String uri = getGameUri(result);
			System.out.println("Connecting to Game with " + uri);
			container.connectToServer(new PlayerEndpoint("p" + result, getGameRule()), URI.create(uri));
			messageLatch.await();
		} catch (DeploymentException | IOException | InterruptedException ex) {
			ex.printStackTrace();
		}
	}

	private static int getPlayerId() {
		// keeping it simple
		Random r = new Random();
		return r.nextInt(100 - 10) + 20;
	}

	private static String getGameUri(int result) {
		Properties properties = getProperties();

		return "ws://" + properties.get("HOSTNAME") + ":" + properties.get("PORT") + "/game/" + "p" + result + "/";
	}

	private static int getGameRule() {
		return GameUtility.toInteger((String) getProperties().get("divisor"));
	}

	private static Properties getProperties() {
		return SystemPropertyLoader.getProperties();
	}
}
