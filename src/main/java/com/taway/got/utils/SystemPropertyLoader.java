package com.taway.got.utils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

public class SystemPropertyLoader {

	static final private String GAME_PROPERTIES = "/config.properties";

	private static Properties properties = null;

	public static Properties getProperties() {
		if (properties == null)
			properties = readProperties();

		return properties;
	}

	private static Properties readProperties() {
		Properties properties = new Properties();
		try {
			properties.load(new InputStreamReader(SystemPropertyLoader.class.getResourceAsStream(GAME_PROPERTIES)));
		} catch (IOException ioe) {
			System.err.println("[ERROR] IOException Error initiating read from  " + GAME_PROPERTIES);
			ioe.printStackTrace();
			System.exit(1);
		}

		return properties;
	}
}
