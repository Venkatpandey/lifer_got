package com.taway.got.utils;

import lombok.AllArgsConstructor;

@AllArgsConstructor public class GameInProgressNotification {

	public static String getGameInProgressNotification() {

		return "There is a game going on already, please wait for your turn.!";
	}

	public static String getPlayerQuitGameNotification(String player) {
		return "Player " + player + " has quit the game";
	}

	public static String getWelcomeMessageNotification(String playerName) {
		return "Welcome " + playerName;
	}

	public static String getGameOverNotification() {
		return "Game Over..!";
	}

	public static String getPlayerLostNotification() {
		return "You lose, thanks for playing.!";
	}
}
