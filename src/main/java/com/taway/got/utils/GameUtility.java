package com.taway.got.utils;

public class GameUtility {

	public static int GAME_POINT = 1;

	public static boolean isNumeric(String strNum) {
		if (strNum == null) {
			return false;
		}
		try {
			double d = Double.parseDouble(strNum);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	public static int toInteger(String message) {
		return Integer.parseInt(message);
	}

	public static String toString(Object num) {
		return String.valueOf(num);
	}
}
