package com.taway.got.exceptions;

public class InvalidMoveException extends Exception {
	public InvalidMoveException(String error) {
		super(error);
	}
}
