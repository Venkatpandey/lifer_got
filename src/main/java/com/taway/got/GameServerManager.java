package com.taway.got;

import com.taway.got.utils.GameUtility;
import com.taway.got.utils.SystemPropertyLoader;
import org.glassfish.tyrus.server.Server;

import javax.websocket.DeploymentException;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public final class GameServerManager {

	private Server gameServer;

	public static void main(String[] args) throws Exception {
		new GameServerManager().startServer();
		new CountDownLatch(1).await();
	}

	private void startServer() throws DeploymentException {
		Properties properties = SystemPropertyLoader.getProperties();
		gameServer = new Server(GameUtility.toString(properties.get("HOSTNAME")),
			GameUtility.toInteger(GameUtility.toString(properties.get("PORT"))), "", null,
			GameModerator.class);

		start();
		addShutdownHookOnClose();
	}

	private void addShutdownHookOnClose() {
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				gameServer.stop();
				System.out.println("Game server Stopped.");

			}
		}));
	}

	public void stop() {
		gameServer.stop();
	}

	private void start() throws DeploymentException {
		gameServer.start();
		System.out.println("Game Server started.!");
		System.out.println("Waiting for players to join.");
	}
}
