package com.taway.got;

import com.taway.got.utils.GameInProgressNotification;
import com.taway.got.utils.GameUtility;
import com.taway.got.utils.SystemPropertyLoader;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.function.Predicate;

@ServerEndpoint(value = "/game/{player}/") public class GameModerator {

	private static final Set<String> PLAYERS = new ConcurrentSkipListSet<>();
	private String player;
	private Session s;
	private boolean noSpotLeft = false;
	private static int MAX_PLAYER_SUPPORT = 2;

	@OnOpen public void registerPlayer(@PathParam("player") String player, Session s) {
		if (PLAYERS.size() == MAX_PLAYER_SUPPORT) {
			try {
				noSpotLeft = true;
				s.getBasicRemote().sendText(GameInProgressNotification.getGameInProgressNotification());
				s.close();
			} catch (IOException ex) {
				System.out.println("No Spot left for new player");
				ex.printStackTrace();
			}
		}

		if (!noSpotLeft) {
			this.s = s;
			s.getUserProperties().put("player", player);
			this.player = player;
			PLAYERS.add(player);
			System.out.println("New player registered : " + player);

			welcomePlayer();
			startGame();
		}
	}

	private void welcomePlayer() {
		try {
			s.getBasicRemote().sendText(GameInProgressNotification.getWelcomeMessageNotification(this.player));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void startGame() {
		if (PLAYERS.size() == MAX_PLAYER_SUPPORT) {
			int start = getRandomNumber();
			System.out.println("Game Started, " + start + " sent to player " + this.player);
			s.getOpenSessions().stream().filter((sn) -> sn.getUserProperties().get("player").equals(this.player))
				.forEach((sn) -> sn.getAsyncRemote().sendText(GameUtility.toString(start)));
		}
	}

	private int getRandomNumber() {
		Properties properties = SystemPropertyLoader.getProperties();
		Random r = new Random();

		return r.nextInt(GameUtility.toInteger(GameUtility.toString(properties.get("maxBound"))) -
			GameUtility.toInteger(GameUtility.toString(properties.get("minBound")))
			+ GameUtility.toInteger(GameUtility.toString(properties.get("minBound"))));
	}

	@OnMessage public void responseReceived(String move, Session s) {
		System.out.println(player + " played : " + move);

		if (GameUtility.isNumeric(move)) {
			if (GameUtility.toInteger(move) != GameUtility.GAME_POINT) {
				Predicate<Session> thisPlayer = (session) -> !session.getUserProperties().get("player").equals(player);
				s.getOpenSessions().stream().filter(thisPlayer).forEach((session) -> session.getAsyncRemote().sendText(GameUtility.toString(move)));
			} else {
				System.out.println(player + " WON." + " GAME OVER..!");
				endGame();
			}
		}

	}

	private void endGame() {
		System.out.println("End the game.!");
		sendGameOverNotification();
		notifyLostPlayer();
		System.out.println("Close all player sessions.");
		closeActiveSessions();
	}

	@OnClose public void playerExitResponse() {
		if (!noSpotLeft) {
			try {
				PLAYERS.remove(this.player);
				s.getOpenSessions().stream().filter(Session::isOpen).forEach(
					(session) -> session.getAsyncRemote().sendText(
						GameInProgressNotification.getPlayerQuitGameNotification(player)));
			} catch (Exception ex) {
				System.out.println(player + " has left the game.");
				ex.printStackTrace();
			}
		}
	}

	private void closeActiveSessions() {
		s.getOpenSessions().stream().filter(Session::isOpen).forEach(session -> {
			try {
				session.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}

	private void notifyLostPlayer() {
		Predicate<Session> thisPlayer = (session) -> !session.getUserProperties().get("player").equals(player);
		s.getOpenSessions().stream().filter(thisPlayer).forEach((session) -> session.getAsyncRemote().
			sendText(GameInProgressNotification.getPlayerLostNotification()));
	}

	private void sendGameOverNotification() {
		s.getOpenSessions().stream().filter(Session::isOpen).forEach(
			(session) -> session.getAsyncRemote().sendText(
				GameInProgressNotification.getGameOverNotification()));
	}
}
